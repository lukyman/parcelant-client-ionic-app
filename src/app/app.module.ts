import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { IonicStorageModule } from '@ionic/storage';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { RecordingViewPageModule } from '../pages/recording-view/recording-view.module';
import { AddParcelPageModule } from '../pages/add-parcel/add-parcel.module';
import { AddParcelItemPageModule } from '../pages/add-parcel-item/add-parcel-item.module';
import { ReceiveViewPageModule } from '../pages/receive-view/receive-view.module';
import { ParcelListComponent } from '../components/parcel-list/parcel-list';
import { ComponentsModule } from '../components/components.module';
import { ReceiveDetailViewPageModule } from '../pages/receive-detail-view/receive-detail-view.module';
import { DispatchViewPageModule } from '../pages/dispatch-view/dispatch-view.module';
import { CollectionViewPageModule } from '../pages/collection-view/collection-view.module';
import { DispatchDetailViewPageModule } from '../pages/dispatch-detail-view/dispatch-detail-view.module';
import { CollectionDetailViewPageModule } from '../pages/collection-detail-view/collection-detail-view.module';
import { DispatchFinalViewPageModule } from '../pages/dispatch-final-view/dispatch-final-view.module';
import { LoginViewPageModule } from '../pages/login-view/login-view.module';
import { AuthProvider } from '../providers/auth/auth';
import { HttpServiceProvider } from '../providers/http/http';
import { StorageServiceProvider } from '../providers/storage/storage';
import { HttpModule } from '@angular/http';
import { UserInfoProvider } from '../providers/user-info/user-info';
import { ParcelRecordProvider } from '../providers/parcel-record/parcel-record';
import { AccessTokenProvider } from '../providers/access-token/access-token';
import { DispatchParcelProvider } from '../providers/dispatch-parcel/dispatch-parcel';
import { CustomerProvider } from '../providers/customer/customer';
import { DispatchProvider } from '../providers/dispatch/dispatch';
import { ReceiveParcelProvider } from '../providers/receive-parcel/receive-parcel';
import { ErrorMessageProvider } from '../providers/error-message/error-message';
import { CarrierProvider } from '../providers/carrier/carrier';
import { TransitUserProvider } from '../providers/transit-user/transit-user';
import { ParcelSelectionProvider } from '../providers/parcel-selection/parcel-selection';


@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__mydb',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    HttpModule,
    LoginViewPageModule,
    RecordingViewPageModule,
    AddParcelPageModule,
    AddParcelItemPageModule,
    ReceiveViewPageModule,
    ReceiveDetailViewPageModule,
    DispatchViewPageModule,
    DispatchDetailViewPageModule,
    DispatchFinalViewPageModule,
    CollectionViewPageModule,
    CollectionDetailViewPageModule,
    ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    HttpServiceProvider,
    HttpModule,
    StorageServiceProvider,
    UserInfoProvider,
    ParcelRecordProvider,
    AccessTokenProvider,
    DispatchParcelProvider,
    CustomerProvider,
    DispatchProvider,
    ReceiveParcelProvider,
    ErrorMessageProvider,
    CarrierProvider,
    TransitUserProvider,
    ParcelSelectionProvider
  ]
})
export class AppModule {}
