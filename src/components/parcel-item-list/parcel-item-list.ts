import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * Generated class for the ParcelItemListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'parcel-item-list',
  templateUrl: 'parcel-item-list.html'
})
export class ParcelItemListComponent {

  text: string;
  @Input('DetailView') detailView;
  @Input('ParcelItems') ParcelItems:any[];
  @Input("SelectedItems") SelectedItems:any[];
  @Output('onParcelItemSelected') onParcelItemSelected = new EventEmitter()
  @Output('onParcelItemUnselected') onParcelItemUnselected = new EventEmitter()

  constructor() {
    console.log('Hello ParcelItemListComponent Component');
    this.text = 'Hello World';
  }
  itemSelected(e,item){
    if(e.checked){
      this.onParcelItemSelected.emit(item)
    }else{
      this.onParcelItemUnselected.emit(item)
    }
  }

  isSelected(itemid):boolean{
    let noItem = -1;
    if(this.SelectedItems==undefined){
      return false;
    }
    return this.SelectedItems.indexOf(itemid)!=noItem

  }

}
