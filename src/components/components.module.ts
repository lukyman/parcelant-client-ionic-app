import { NgModule } from '@angular/core';
import { ParcelListComponent } from './parcel-list/parcel-list';
import { ParcelItemListComponent } from './parcel-item-list/parcel-item-list';
import { IonicModule } from 'ionic-angular';
@NgModule({
	declarations: [ParcelListComponent,
    ParcelItemListComponent],
	imports: [IonicModule],
	exports: [ParcelListComponent,
    ParcelItemListComponent]
})
export class ComponentsModule {}
