import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { DispatchParcelProvider } from '../../providers/dispatch-parcel/dispatch-parcel';
import { TransitUserProvider } from '../../providers/transit-user/transit-user';
import { CarrierProvider } from '../../providers/carrier/carrier';
import { ParcelSelectionProvider } from '../../providers/parcel-selection/parcel-selection';

/**
 * Generated class for the DispatchFinalViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dispatch-final-view',
  templateUrl: 'dispatch-final-view.html',
})
export class DispatchFinalViewPage implements OnInit {

  private carrierid:number;
  private transitAppUserId:number;

  ngOnInit(): void {
    this.parcelSelection.StorageKey = "ParcelSelectedForDispatch"
    this.getCarriers()
    this.getTransitUser()
  }
  public carriers:any[];
  public transitUsers:any[];
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public events:Events,
              public carrierProvider: CarrierProvider,
              public transitUserProvider: TransitUserProvider, 
              public dispatchParcel:DispatchParcelProvider,
              public parcelSelection:ParcelSelectionProvider) {

                this.events.subscribe("ParcelDispatch:Prepared",out=>{
                  console.log("post dispatch", out)
                  this.postDispatchParcel(out)
                })
               
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DispatchFinalViewPage');
  }

  getCarriers(){
    this.carrierProvider.getCarriers().subscribe(carriers=>{
      this.carriers = carriers.data;
    },error=>{
      throw error
    })
  }
  getTransitUser(){
    this.transitUserProvider.getTransitUsers().subscribe(users=>{
      this.transitUsers = users.data
      console.log(this.transitUsers)
    }, error=>{
      throw error
    })
  }


  postDispatchParcel(parcelDistpatchData){

    this.dispatchParcel.postDispatchParcel(parcelDistpatchData,this.transitAppUserId,this.carrierid)
            .subscribe(data=>{
              console.log(data)
            })
   
  }

  prepareDispatch(formData){
    this.carrierid = formData.carrierId;
    this.transitAppUserId = formData.transitAppUserId;

    let parcelDistpatchData = []
    
    this.parcelSelection.getParcelsSelected().then(data=>{
      console.log("parcels to dispatch",data)

      if(data!=null){
        let counter =0;
        for (const parcel in data) {
          counter +=1;
          let parcelData = {}
          if (data.hasOwnProperty(parcel)) {
            const element = data[parcel];
            console.log(element)

            parcelData["parcelId"] = parcel

            parcelData["parcelItems"]
            let parcelItems= []
            for (let index = 0; index < data[parcel].length; index++) {
              const item = data[parcel][index];
              parcelItems.push({"parcelitemid":item})
              
            }
            
            parcelData["parcelItems"] = parcelItems

            parcelDistpatchData.push(parcelData)
            
           

            if(Object.keys(data).length==counter){

              this.events.publish("ParcelDispatch:Prepared",parcelDistpatchData)
              console.log("finished",parcelDistpatchData)
            }
          }

         

          
        }
      }
    })
  }


}
