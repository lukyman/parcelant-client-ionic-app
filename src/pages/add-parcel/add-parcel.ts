import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { ParcelRecordProvider } from '../../providers/parcel-record/parcel-record';
import { CustomerProvider } from '../../providers/customer/customer';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserInfoProvider } from '../../providers/user-info/user-info';

/**
 * Generated class for the AddParcelPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-parcel',
  templateUrl: 'add-parcel.html',
})
export class AddParcelPage implements OnInit {
  ngOnInit(): void {
    this.getRoute();
    this.getCollectionPoint();
    this.parcelForm();
  }
  createParcelForm:FormGroup;
  customerChecked:boolean;
  routes:any[]
  collections:any[]
  senderNumber;
  senderName;
  receiverNumber;
  receiverName;
  parcelFields:{}
  senderId:number;
  receiverId:number;
  senderExist:boolean=true;
  receiverExist:boolean=true;
  customerCreatedCounter:number=0;


  constructor(public navCtrl: NavController, private fromBuilder:FormBuilder, public navParams: NavParams, 
    public parcelRecord:ParcelRecordProvider, public customer:CustomerProvider,
     public appUser:UserInfoProvider, public events:Events) {
    this.customerChecked = false;

    this.events.subscribe("customer:created",out=>{
      console.log(this.customerCreatedCounter)
      if(this.senderId>0&&this.receiverId>0){
        
        this.createParcel();
      }
      
    })
    this.events.subscribe("parcel:recorded", out=>{
      this.navCtrl.pop()
    })
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddParcelPage');
  }

  checkCustomer(){
    this.senderNumber=this.createParcelForm.value.senderNumber;
    this.receiverNumber=this.createParcelForm.value.receiverNumber;

    console.log(this.senderNumber)
    
    if(this.senderNumber && this.receiverNumber){
    this.customerChecked =true;
    
    this.customer.getCustomerByPhone(this.senderNumber).subscribe(customer=>{
      if(customer.data){
      this.senderNumber = customer.data.phone;
      this.senderName = customer.data.fullName;
      this.senderId = customer.data.id;
      this.customerChecked = this.customerChecked==false?true:true;
     
      }else{
        this.senderExist = false;
          
        
      }
    },error=>{throw error})

    this.customer.getCustomerByPhone(this.receiverNumber).subscribe(customer=>{
      if(customer.data){
      this.receiverNumber = customer.data.phone;
      this.receiverName = customer.data.fullName;
      this.receiverId = customer.data.id;
      this.customerChecked = this.customerChecked==false?true:true;

     
      }else{
        this.receiverExist=false;
      
      }
    },error=>{throw error})
  }

  }

  prepareParcel(){
    console.log(this.createParcelForm.value)
    if(!this.senderExist||!this.receiverExist){
      
      if(!this.senderExist){
        console.log("create sender")
        this.createSender(this.createParcelForm.value.senderName,
                            this.createParcelForm.value.senderNumber)
      }
      
      if(!this.receiverExist){
        console.log("create receiver")
        this.createReceiver(this.createParcelForm.value.receiverName,
                            this.createParcelForm.value.receiverNumber)
      }

    }else{
      this.createParcel();
    }
  }

  createParcel(){
    console.log("form valid?",this.createParcelForm.valid)
      if(this.createParcelForm.valid){
        console.log(this.createParcelForm.value)
      
        this.parcelFields={
          clientId: this.appUser.UserInfo.clientId,
          clientBranchId :this.appUser.UserInfo.branchId,
          appuserId: this.appUser.UserInfo.id,
          senderId: this.senderId,
          receiverId: this.receiverId,
          routeId: this.createParcelForm.value.routeId,
          collectionpointid:this.createParcelForm.value.collectionId,
        }
        
        this.parcelRecord.createParcel(this.parcelFields).subscribe(parcel=>{
          if(parcel.data){
            this.events.publish("parcel:recorded")
          }
        },error=>{

        })
      }
  }
 
  getRoute(){
    this.parcelRecord.getRoutes().subscribe(route=>this.routes=route.data,error=>{
      throw error
    })
  }
  getCollectionPoint(){
    this.parcelRecord.getCollection().subscribe(collection=>this.collections=collection.data,
    error=>{
      throw error
    })
  }
  parcelForm(){
    this.createParcelForm =this.fromBuilder.group({
      senderName : ['',Validators.required],
      senderNumber : ['',[Validators.required,Validators.minLength(10)]],
      receiverName : ['',Validators.required],
      receiverNumber : ['',[Validators.required,Validators.minLength(10)]],
      routeId: ['',Validators.required],

    })
  }
 

  createSender(name,number){
    let customer ={
      fullName:name,
      phone:number
    }
    this.customer.createCustomer(customer).subscribe(success=>{
      if(success.data){
        //this.customerCreatedCounter-=1;
         this.senderId = success.data.id;
        console.log("sender:created")

        // increment the number of customers has been created
        this.customerCreatedCounter+=1;
        this.events.publish("customer:created")
      }
    },error=>{

    })
  }
  createReceiver(name,number){
    let customer ={
      fullName:name,
      phone:number
    }
    this.customer.createCustomer(customer).subscribe(success=>{
      if(success.data){
        //this.customerCreatedCounter-=1;
        this.receiverId = success.data.id;
        console.log("receiver:created")

        // increment the number of customers has been created
        this.customerCreatedCounter+=1;
        this.events.publish("customer:created")
      }
    },error=>{

    })

  }
}
