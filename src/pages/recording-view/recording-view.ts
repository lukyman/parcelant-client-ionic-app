import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { NewParcelTabPage } from '../new-parcel-tab/new-parcel-tab';
import { RecentParcelTabPage } from '../recent-parcel-tab/recent-parcel-tab';

/**
 * Generated class for the RecordingViewPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  defaultHistory:['HomePage']
})
@Component({
  selector: 'page-recording-view',
  templateUrl: 'recording-view.html'
})
export class RecordingViewPage {
  
  newParcelTabRoot = 'NewParcelTabPage'
  recentParcelTabRoot = 'RecentParcelTabPage'
  


  constructor(public navCtrl: NavController) {}
 
}
