import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddParcelItemPage } from './add-parcel-item';

@NgModule({
  declarations: [
    AddParcelItemPage,
  ],
  imports: [
    IonicPageModule.forChild(AddParcelItemPage),
  ],
})
export class AddParcelItemPageModule {}
