import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DispatchViewPage } from './dispatch-view';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    DispatchViewPage,
  ],
  imports: [
    IonicPageModule.forChild(DispatchViewPage),
    ComponentsModule
  ],
})
export class DispatchViewPageModule {}
