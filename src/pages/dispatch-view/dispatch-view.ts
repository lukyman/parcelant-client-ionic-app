import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { DispatchDetailViewPage } from '../dispatch-detail-view/dispatch-detail-view';
import { DispatchParcelProvider } from '../../providers/dispatch-parcel/dispatch-parcel';
import { ParcelSelectionProvider } from '../../providers/parcel-selection/parcel-selection';

/**
 * Generated class for the DispatchViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dispatch-view',
  templateUrl: 'dispatch-view.html',
})
export class DispatchViewPage implements OnInit{
  ngOnInit(): void {
    this.parcelSelection.ParcelSelectedNotificationName = this.ParcelSelectedForDispatch;
    this.parcelSelection.ParcelUnSelectedNotificationName = this.ParcelUnSelectedForDispatch;
    this.parcelSelection.StorageKey = "ParcelSelectedForDispatch"
    this.getParcels()
    this.getSelectedParcels()

  }
  public DetailView = DispatchDetailViewPage;
  public ParcelData:parcel[];
  public SelectedParcel:any[];
  public totalParcelsSelected:number =0;
  public totalItemsSelected:number=0;
  private ParcelSelectedForDispatch:string = "ParcelSelectedForDispatch";
  private ParcelUnSelectedForDispatch:string = "ParcelUnSelectedForDispatch";

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public dispatchParcel:DispatchParcelProvider, 
              public events:Events,
              public parcelSelection:ParcelSelectionProvider) {
      
               

     this.events.subscribe(this.ParcelSelectedForDispatch,out=>{
       this.getSelectedParcels()
     })

     this.events.subscribe(this.ParcelUnSelectedForDispatch,out=>{
      this.getSelectedParcels()
     })

     this.events.subscribe("Parcel:ItemSelectedFirst",out=>{
       this.getSelectedParcels();
     })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DispatchViewPage');
  }

  goto(intent){
    this.navCtrl.push(`${intent}Page`)
  }

  getParcels(){
    this.dispatchParcel.getParcelsToDispatch().subscribe(parcels=>{
      this.ParcelData=parcels.data
      this.ParcelData.reverse()
    },
    error=>{
      throw error
    })
  }
 
  parcelUnselected(data){
    console.log("parcel Unselected", data)
    this.parcelSelection.removeParcel(data.id);
  }

  parcelSelected(data){
    this.dispatchParcel.getPracelItemsToDispatch(data.id).subscribe(items=>{
     let itemids = []
     let length = items.data.length
      items.data.forEach(element => {
        itemids.push(element.id)
        if(itemids.length==length){
          this.parcelSelection.addSelectedParcel(data.id,itemids)
        }
      });  
    
    })
   
  }
  getSelectedParcels(){
    this.parcelSelection.getParcelsSelected()
    .then(data=>{
      this.SelectedParcel = data
      if(data){
        this.SelectedParcel = data
      this.totalParcelsSelected = Object.keys(data).length
      this.totalItemsSelected =0;
      for (const key in data) {
        if (data.hasOwnProperty(key)) {
          this.totalItemsSelected += data[key].length;
          
        }
      }
    }else{
      this.SelectedParcel = []
    }
    })
  }

  postDispatchParcel(){

    this.parcelSelection.getParcelsSelected().then(data=>{
      console.log("parcels to dispatch",data)
    })
  }

  prepareDispatch(){

  }
  
}
