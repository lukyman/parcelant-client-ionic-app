import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceiveDetailViewPage } from './receive-detail-view';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ReceiveDetailViewPage,
  ],
  imports: [
    IonicPageModule.forChild(ReceiveDetailViewPage),
    ComponentsModule
  ],
})
export class ReceiveDetailViewPageModule {}
