import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ReceiveParcelProvider } from '../../providers/receive-parcel/receive-parcel';
import { ParcelSelectionProvider } from '../../providers/parcel-selection/parcel-selection';

/**
 * Generated class for the ReceiveDetailViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-receive-detail-view',
  templateUrl: 'receive-detail-view.html',
})
export class ReceiveDetailViewPage implements OnInit{

  ngOnInit(): void {
    this.getItemToReceive()
    this.getSelectedItems()
  }
  Parcel:parcel;
  ParcelItems:any[];
  SelectedItems:any[];

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public receiveParcel:ReceiveParcelProvider,
    public parcelSelection:ParcelSelectionProvider  
  ) {

    this.Parcel = this.navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReceiveDetailViewPage');
  }

  getItemToReceive(){
    this.receiveParcel.getParcelItemsToReceive(this.Parcel.id)
      .subscribe(items=>{
        this.ParcelItems = items.data
      },error=>{
        throw error
      })
  }
  getSelectedItems(){
    this.parcelSelection.getSelectedParcelItems(this.Parcel.id).then(parent=>{
      console.log("items",parent)
      this.SelectedItems = parent
    })
  }

}
