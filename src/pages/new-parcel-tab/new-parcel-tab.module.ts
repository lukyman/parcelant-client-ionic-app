import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewParcelTabPage } from './new-parcel-tab';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    NewParcelTabPage,
  ],
  imports: [
    IonicPageModule.forChild(NewParcelTabPage),
    ComponentsModule
  ],
})
export class NewParcelTabPageModule {}
