import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CollectionViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-collection-view',
  templateUrl: 'collection-view.html',
})
export class CollectionViewPage {

  searchQuery: string = '';
  ParcelData: any[];
  DetailView:"CollectionDetailView";
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.initializeItems();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CollectionDetailViewPage');
  }
 

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.ParcelData = this.ParcelData.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  } 
  initializeItems() {
    this.ParcelData = [{
      "totalItems": 0,
      "totalItemsDispatch": 0,
      "parcelStatusDetailId": 6,
      "parcelStatusDetailName": "Recorded",
      "id": 30,
      "clientId": 1,
      "clientBranchId": 4,
      "clientBranchName": "moyale biashara",
      "appUserId": 1,
      "appUserName": "babu owino",
      "parcelNumber": "019",
      "senderName": "Ahmed",
      "senderPhone": "254708901215",
      "receiverName": "adano",
      "receiverPhone": "254708901214",
      "routesId": 1,
      "routeName": "Nairobi to moyale biashra",
      "collectionPointId": 3,
      "collectionPointName": "Biashara street",
      "createdAt": "2018-03-07T14:15:37.027",
      "updatedAt": "2018-03-07T14:15:37.027"
  },
  {
      "totalItems": 0,
      "totalItemsDispatch": 0,
      "parcelStatusDetailId": 7,
      "parcelStatusDetailName": "Recorded",
      "id": 31,
      "clientId": 1,
      "clientBranchId": 4,
      "clientBranchName": "moyale biashara",
      "appUserId": 1,
      "appUserName": "babu owino",
      "parcelNumber": "020",
      "senderName": "Ahmed",
      "senderPhone": "254708901215",
      "receiverName": "abdi",
      "receiverPhone": "254708901213",
      "routesId": 1,
      "routeName": "Nairobi to moyale biashra",
      "collectionPointId": 1,
      "collectionPointName": "self",
      "createdAt": "2018-03-07T17:48:22.683",
      "updatedAt": "2018-03-07T17:48:22.683"
  },{
    "totalItems": 0,
    "totalItemsDispatch": 0,
    "parcelStatusDetailId": 6,
    "parcelStatusDetailName": "Recorded",
    "id": 30,
    "clientId": 1,
    "clientBranchId": 4,
    "clientBranchName": "moyale biashara",
    "appUserId": 1,
    "appUserName": "babu owino",
    "parcelNumber": "019",
    "senderName": "Ahmed",
    "senderPhone": "254708901215",
    "receiverName": "adano",
    "receiverPhone": "254708901214",
    "routesId": 1,
    "routeName": "Nairobi to moyale biashra",
    "collectionPointId": 3,
    "collectionPointName": "Biashara street",
    "createdAt": "2018-03-07T14:15:37.027",
    "updatedAt": "2018-03-07T14:15:37.027"
},
{
    "totalItems": 0,
    "totalItemsDispatch": 0,
    "parcelStatusDetailId": 7,
    "parcelStatusDetailName": "Recorded",
    "id": 31,
    "clientId": 1,
    "clientBranchId": 4,
    "clientBranchName": "moyale biashara",
    "appUserId": 1,
    "appUserName": "babu owino",
    "parcelNumber": "020",
    "senderName": "Ahmed",
    "senderPhone": "254708901215",
    "receiverName": "abdi",
    "receiverPhone": "254708901213",
    "routesId": 1,
    "routeName": "Nairobi to moyale biashra",
    "collectionPointId": 1,
    "collectionPointName": "self",
    "createdAt": "2018-03-07T17:48:22.683",
    "updatedAt": "2018-03-07T17:48:22.683"
}
      
    ];
  }
}
