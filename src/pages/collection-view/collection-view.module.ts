import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CollectionViewPage } from './collection-view';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CollectionViewPage,
  ],
  imports: [
    IonicPageModule.forChild(CollectionViewPage),
    ComponentsModule
  ],
})
export class CollectionViewPageModule {}
