import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, LoadingController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { HomePage } from '../home/home';
import { UserInfoProvider } from '../../providers/user-info/user-info';

/**
 * Generated class for the LoginViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login-view',
  templateUrl: 'login-view.html',
})
export class LoginViewPage {

  
  constructor(public navCtrl: NavController, public navParams: NavParams,public events:Events,
    public userInfoProvider:UserInfoProvider,public authProvider:AuthProvider, public loadingView:LoadingController) {


      this.events.subscribe('token:initalized',out=>{
        this.getUserInfo();
      })

      this.events.subscribe('userinfo:initialized',out=>{
       
        this.navCtrl.setRoot(HomePage);
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginViewPage');
  }

 
  login(data){
    
    if(data.username && data.password){
      this.showLoading();
    this.authProvider.login(data).subscribe(data=>{
        console.log(data.access_token)
       this.saveAccessToken(data.access_token)
    },error=>{
        console.log(error)
    });
    }
  }

  saveAccessToken(token:string){
    console.log("about to save")
    this.authProvider.saveAccessToken(token).then(t=>{
      console.log("token:saved")
      this.events.publish("token:saved");
    })
  }

  getUserInfo(){
    this.userInfoProvider.fetchUserInfo().subscribe(success=>{
      console.log(success.error)
      this.authProvider.saveUserAuth(success.data)
      
    })
  }

  private showLoading(){
   let loading = this.loadingView.create({
      content:"loging in",
      enableBackdropDismiss:false,
      dismissOnPageChange:true
      
    });

    loading.present();

   
  }

}
