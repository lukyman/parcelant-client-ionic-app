import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CollectionDetailViewPage } from './collection-detail-view';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CollectionDetailViewPage,
  ],
  imports: [
    IonicPageModule.forChild(CollectionDetailViewPage),
    ComponentsModule
  ],
})
export class CollectionDetailViewPageModule {}
