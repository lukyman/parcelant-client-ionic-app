import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RecentParcelTabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recent-parcel-tab',
  templateUrl: 'recent-parcel-tab.html',
})
export class RecentParcelTabPage {
  

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecentParcelTabPage');
  }
  goto(intent){
    this.navCtrl.push(`${intent}Page`)
  }
}
