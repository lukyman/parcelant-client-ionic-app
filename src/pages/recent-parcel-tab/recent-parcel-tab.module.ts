import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecentParcelTabPage } from './recent-parcel-tab';

@NgModule({
  declarations: [
    RecentParcelTabPage,
  ],
  imports: [
    IonicPageModule.forChild(RecentParcelTabPage),
  ],
})
export class RecentParcelTabPageModule {}
