interface appuser{
    active:boolean;
    branchId:number;
    branchName:string;
    clientId:number;
    clientName:string;
    createdAt:string;
    fullname:string;
    id:number;
    phone:string;
    updatedAt:string;
    username:string
  }

interface AppUserSingleResponse{
    error:boolean;
    errorMessage:object;
    status:number;
    data:appuser;
}


interface AppUserListResponse{
    error:boolean;
    errorMessage:object;
    status:number;
    data:appuser;
}