import { Injectable, Injector } from '@angular/core';
import { StorageServiceProvider } from '../storage/storage';
import { Http,Headers, Response, } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';
import { Events } from 'ionic-angular';
import { AccessTokenProvider } from '../access-token/access-token';

/*
  Generated class for the HttpProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HttpServiceProvider {
  
  private baseUrl: any;
 
  private test = false;
  private access_token:string;
  
  constructor(public http: Http, injector: Injector, public accessTokenProvider: AccessTokenProvider,
     private events: Events) {

    if (this.test) {
      this.baseUrl = 'https://api.parcelant.com/';
    } else {
      this.baseUrl = 'https://api.parcelant.com/';
    }
    console.log(this.baseUrl)
    console.log('Hello ProvidersHttpProvider Provider');
  
  }
  emitTokenExpiredEvent() {
    this.events.publish('userAuth:expired');
  } 

  /* for some reason the api is using POST instead of GET to fetch resource*/
  login(endpoint,postData) {
    console.log(endpoint)
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    let fullurl = `${this.baseUrl}${endpoint}`;
    return this.http.post(fullurl, postData, {headers:headers}).map(data=>this.extractData(data),(error)=>this.handleError(error));
   
  }


  /**
   * Get data that needs authentication
   */
  Get(endpoint) {
   
    console.log("access_token", this.accessTokenProvider.token)
     
      // convert token to base64
      let auth = `Bearer ${this.accessTokenProvider.token}`;
     
      let headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      
      headers.append('Authorization', auth)
      
      // endpoint
      let fullurl = `${this.baseUrl}${endpoint}`;
     
      
      //make http post request
      return this.http.get(fullurl, { headers: headers }).map(data => this.extractData(data)).catch((error) =>  this.handleError(error));
  
  }


  
  /**
   * Get data that needs authentication
   */
  Post(endpoint, postData) {
    
    console.log("access_token", this.accessTokenProvider.token)
         
      // convert token to base64
      let auth = `Bearer ${this.accessTokenProvider.token}`;
         
      let headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
          
      headers.append('Authorization', auth)
          
      // endpoint
      let fullurl = `${this.baseUrl}${endpoint}`;
    
      // request body
      let body = this.encodeObject(postData).toString();
      console.log(body)
      //make http post request
      return this.http.post(fullurl, body, { headers: headers }).map(data => this.extractData(data)).catch((error) =>  this.handleError(error));
  
  }

  PostJson(endpoint,postData){
    console.log("access_token", this.accessTokenProvider.token)
         
    // convert token to base64
    let auth = `Bearer ${this.accessTokenProvider.token}`;
       
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
        
    headers.append('Authorization', auth)
        
    // endpoint
    let fullurl = `${this.baseUrl}${endpoint}`;
  
    // request body
    
    console.log(postData)
    //make http post request
    return this.http.post(fullurl, postData, { headers: headers }).map(data => this.extractData(data)).catch((error) =>  this.handleError(error));

  }
  
  private extractData(res: Response) {
    console.log(res.json())
    return res.json() || {}
  }
  
  private handleError(error: Response) {
   
    if (error.status === 401) {
      this.events.publish('userAuth:expired');
      //emitTokenExpiredEvent();
    } else {

  
      console.log(JSON.stringify(error))
      return  error.json();
    }
}  
  
encodeObject(obj): URLSearchParams{
    let params: URLSearchParams = new URLSearchParams();
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            params.append(key, obj[key]);
        }
    }
    return params;
}

}
