import { Injectable, Injector } from '@angular/core';
import { HttpServiceProvider } from '../http/http';
import { StorageServiceProvider } from '../storage/storage';
import { Events } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  
  userinfo: appuser;
  access_token:string;
  constructor(public httpService: HttpServiceProvider,private storage: StorageServiceProvider, 
    injector:Injector, private events:Events) {

    console.log('Hello AuthserviceProvider Provider');
    this.getUserAuth();

    this.events.subscribe('user:loggedout', out => {
      this.initializeUserAuth();  
    })

  
    this.initializeUserAuth();
  }
  
  initializeUserAuth() {
    this.storage.get('userinfo').then(data => {
      this.userinfo = data;
      this.storage.get("access_token").then(token=>this.access_token = token);
      console.log("userinfo  initialized", this.userinfo);
    })
  }

  login(data) {
    data.grant_type = "password";
    let logindata = this.httpService.encodeObject(data).toString();
    console.log(logindata)
    return this.httpService.login("connect/token",logindata);
  }

  logout() {
    this.storage.remove('userinfo');
    this.storage.remove('access_token');
    
  }

  fetchUserInfo():Observable<AppUserSingleResponse>{
   return this.httpService.Get('api/appuserinfo');
  }


  saveUserAuth(userAuth) {
    this.storage.set('userinfo', userAuth).then(x=>{
      this.events.publish('userinfo:saved')
    });
  }  

  saveAccessToken(access_token:string){
    return this.storage.set("access_token",access_token)
  }

  isAuthenticated():Boolean {
    console.log(this.userinfo)
  
    if (this.userinfo && this.access_token!='') { 
      return true;

    } else {
      return false;
    }
  }
  
  getUserAuth() {
    return this.storage.get('userAuth').then(data => {
      if (data) {
       return this.userinfo = data;
      } else {
        return {}  
      }  
    });
    }
}
 


