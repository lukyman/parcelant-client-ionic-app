import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpServiceProvider } from '../http/http';

/*
  Generated class for the CustomerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CustomerProvider  {

  private customerPath = 'api/customer';

  constructor(private httpService: HttpServiceProvider) {
    console.log('Hello CustomerProvider Provider');
  }
  createCustomer(customer){
    return this.httpService.Post(`${this.customerPath}`,customer);
  }
  getCustomerByPhone(phone){
    return this.httpService.Get(`${this.customerPath}?phone=${phone}`)
  }

}
