import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpServiceProvider } from '../http/http';
import { UserInfoProvider } from '../user-info/user-info';

/*
  Generated class for the CarrierProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CarrierProvider {


  constructor(public httpProvider: HttpServiceProvider,
              public userInfoProvider:UserInfoProvider) {
    console.log('Hello CarrierProvider Provider');
  }

  getCarriers(){
    return this.httpProvider.Get(`api/Carrier/${this.userInfoProvider.UserInfo.clientId}`)
  }

}
