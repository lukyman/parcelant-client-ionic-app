import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { StorageServiceProvider } from '../storage/storage';

/*
  Generated class for the ParcelSelectionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ParcelSelectionProvider {

  public StorageKey:string;
  public ParcelSelectedNotificationName:string;
  public ParcelUnSelectedNotificationName:string;

  constructor(public events:Events,
              public storageProvider:StorageServiceProvider) {
    console.log('Hello ParcelSelectionProvider Provider');

    
  }


  addSelectedParcel(parcelid, itemsid) {
    console.log(this.StorageKey)
    this.storageProvider.get(this.StorageKey).then(data => {
      
        let _data = data == null ? {} : data;
        console.log(_data)
        if (_data[parcelid] == null) {
          _data[parcelid] = itemsid
          data = _data
          this.storageProvider.set(this.StorageKey, data).then(success => {
            this.events.publish(this.ParcelSelectedNotificationName)
          })
        }

      
    })
  }

  addSelectedParcelItem(parcelid, itemid) {
    this.storageProvider.get(this.StorageKey).then(data => {
      data = data==null?{}:data
      if (data[parcelid]!=null) {
        let _data: any[] = data[parcelid]

        // not items array
        if (!_data) {
          let items = []
          items.push(itemid)

          data[parcelid] = items
          console.log(data)
          this.storageProvider.set(this.StorageKey, data)

        } else {
          // item dont exist
          if (_data.indexOf(itemid) == -1) {
            console.log("found", data)
            _data.push(itemid);
            data[parcelid] = _data;
            this.storageProvider.set(this.StorageKey, data)
          }


        }
      } else {
        
      
          let items = [itemid]; 
        
          data[parcelid]= items
        
        console.log(data)
        this.storageProvider.set(this.StorageKey, data)
        this.events.publish("Parcel:ItemSelectedFirst")
      }
    })
  }

  removeParcel(parcelid) {
    this.storageProvider.get(this.StorageKey).then(data => {
      if (data[parcelid]) {
        delete data[parcelid]
        this.storageProvider.set(this.StorageKey, data).then(success => {
          this.events.publish(this.ParcelUnSelectedNotificationName)
        })

      }
    })
  }

  removeParcelItem(parcelid, itemid) {
    this.storageProvider.get(this.StorageKey).then(data => {
      if (data) {

        let items: any[] = data[parcelid]
        console.log(data, items)
        if (items.length > 0) {
          // item exist then remove
          if (items.indexOf(itemid) != -1) {
            items = this.removeElement(items, itemid)
            data[parcelid] = items;
            this.storageProvider.set(this.StorageKey, data)
          }
        } else
          delete data[parcelid]
        this.storageProvider.set(this.StorageKey, data)
      }



    })
  }

  clearParcelsSelected() {
    this.storageProvider.remove(this.StorageKey)
  }

  getParcelsSelected() {
    console.log(this.StorageKey)
    return this.storageProvider.get(this.StorageKey)
  }
  getSelectedParcelItems(parcelid) {
    return this.storageProvider.get(this.StorageKey).then(data => {
      if (data) {
        return data[parcelid]
      }
      return [];
    })
  }


  private removeElement(array, element) {
    if (array) {
      const index = array.indexOf(element);
      array.splice(index, 1);
      return array
    }

  }

}
