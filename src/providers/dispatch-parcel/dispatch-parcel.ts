import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpServiceProvider } from '../http/http';
import { UserInfoProvider } from '../user-info/user-info';
import { StorageServiceProvider } from '../storage/storage';
import { Events } from 'ionic-angular';
import { CarrierProvider } from '../carrier/carrier';
import { TransitUserProvider } from '../transit-user/transit-user';

/*
  Generated class for the DispatchParcelProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DispatchParcelProvider {

  DispatchPath = 'api/dispatch';
  StorageKey = "selectedfordispatch";

  clientId: number;
  branchId: number;
  constructor(public httpService: HttpServiceProvider,
    public userInfo: UserInfoProvider,
    public storageProvider: StorageServiceProvider,
    private events: Events) {
    console.log('Hello DispatchParcelProvider Provider');
    this.clientId = this.userInfo.UserInfo.clientId;
    this.branchId = this.userInfo.UserInfo.branchId;
  }

  getParcelsToDispatch() {
    return this.httpService.Get(`${this.DispatchPath}/parcelstodispatch/${this.clientId}/${this.branchId}`)
  }

  getPracelItemsToDispatch(parcelId) {
    return this.httpService.Get(`api/Dispatch/parcelitemstodispatch/${this.clientId}/${this.branchId}/${parcelId}`)
  }

  postDispatchParcel(parcels,transitAppUserId,carrierid){
    let data = {
        appUserId:this.userInfo.UserInfo.id,
        updateParcelStatus:parcels
    }

    let extraPath=`transitAppUserId=${transitAppUserId}&carrierid=${carrierid}`

   return this.httpService.PostJson(`api/Dispatch/${this.clientId}/${this.branchId}?${extraPath}`,data);
  }



  /*addSelectedParcelForDispatch(parcelid, itemsid) {
    this.storageProvider.get(this.StorageKey).then(data => {
      if (data) {
        let _data = data == null ? {} : data;
        console.log(_data)
        if (_data[parcelid] == null) {
          _data[parcelid] = itemsid
          data = _data
          this.storageProvider.set(this.StorageKey, data).then(success => {
            this.events.publish("ParcelSelectedForDispatch")
          })
        }

      }
    })
  }

  addSelectedParcelItemForDispatch(parcelid, itemid) {
    this.storageProvider.get(this.StorageKey).then(data => {
      if (data) {
        let _data: any[] = data[parcelid]

        // not items array
        if (!_data) {
          let items = []
          items.push(itemid)

          data[parcelid] = items
          console.log(data)
          this.storageProvider.set(this.StorageKey, data)

        } else {
          // item dont exist
          if (_data.indexOf(itemid) == -1) {
            console.log("found", _data)
            _data.push(itemid);
            data = _data;
            this.storageProvider.set(this.StorageKey, data)
          }


        }
      } else {
        let items = [itemid]
        data = {
          [parcelid]: items
        }
        console.log(data)
        this.storageProvider.set(this.StorageKey, data)
      }
    })
  }

  removeParcelFromDispatch(parcelid) {
    this.storageProvider.get(this.StorageKey).then(data => {
      if (data[parcelid]) {
        delete data[parcelid]
        this.storageProvider.set(this.StorageKey, data).then(success => {
          this.events.publish("ParcelUnSelectedForDispatch")
        })

      }
    })
  }

  removeParcelItemFromDispatch(parcelid, itemid) {
    this.storageProvider.get(this.StorageKey).then(data => {
      if (data) {

        let items: any[] = data[parcelid]
        console.log(data, items)
        if (items.length > 0) {
          // item exist then remove
          if (items.indexOf(itemid) != -1) {
            items = this.removeElement(items, itemid)
            data[parcelid] = items;
            this.storageProvider.set(this.StorageKey, data)
          }
        } else
          delete data[parcelid]
        this.storageProvider.set(this.StorageKey, data)
      }



    })
  }

  clearParcelsSelectedForDispatch() {
    this.storageProvider.remove(this.StorageKey)
  }

  getParcelsSelectedForDispatch() {
    return this.storageProvider.get(this.StorageKey)
  }
  getSelectedParcelItems(parcelid) {
    return this.storageProvider.get(this.StorageKey).then(data => {
      if (data) {
        return data[parcelid]
      }
      return [];
    })
  }


  private removeElement(array, element) {
    if (array) {
      const index = array.indexOf(element);
      array.splice(index, 1);
      return array
    }

  }*/


}
