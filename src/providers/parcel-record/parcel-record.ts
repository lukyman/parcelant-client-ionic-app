import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpServiceProvider } from '../http/http';
import { UserInfoProvider } from '../user-info/user-info';

/*
  Generated class for the ParcelRecordProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ParcelRecordProvider {

  private parcelPath = "api/parcel";
  private parcelItemPath = "api/parcelitem"
  clientId:number;
  branchId:number;

  constructor(public httpService:HttpServiceProvider, public userInfoProvider:UserInfoProvider) {
    console.log('Hello ParcelRecordProvider Provider');
    this.clientId = this.userInfoProvider.UserInfo.clientId;
    this.branchId = this.userInfoProvider.UserInfo.branchId;
  }

  getAll(){
    console.log("userinfo",this.userInfoProvider.UserInfo)
    return this.httpService.Get(`${this.parcelPath}/${this.userInfoProvider.UserInfo.clientId}`)
  }
  
  getById(){

  }

  getparcelItems(parcelId){
    return this.httpService.Get(`${this.parcelItemPath}/${this.clientId}/${parcelId}`)
  }

  getRoutes(){
    return this.httpService.Get(`api/route/destination/client/${this.clientId}/branch/${this.branchId}`)
  }
  
  getCollection(){
    return this.httpService.Get(`api/collectionpoint/${this.clientId}`)
  }

  
  createParcel(parcel){
    return this.httpService.Post(`${this.parcelPath}`,parcel);
  }
  AddParcelItem(parcelId,item){
    item['clientid']=this.clientId;
    item['parcelid']= parcelId;
    item['appuserid']=this.userInfoProvider.UserInfo.id;
    
    return this.httpService.Post(`api/ParcelItem/${this.clientId}/${parcelId}`,item)
  }

}
